class UserModel {
  late int _UserID;

  int get UserID => _UserID;

  set UserID(int value) {
    _UserID = value;
  }

  late String _Name;

  String get Name => _Name;

  set Name(String value) {
    _Name = value;
  }

  late String _City;

  String get City => _City;

  set City(String value) {
    _City = value;
  }

  UserModel.name(this._UserID, this._Name, this._City);
}
