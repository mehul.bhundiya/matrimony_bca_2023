import 'package:flutter/material.dart';
import 'package:matrimony/api/api_call.dart';
import 'package:matrimony/api_integration/get_user_list_page.dart';
import 'package:matrimony/database/demo_database.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  MyApp({super.key});

  DemoDatabase db = DemoDatabase();

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Matrimony',
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        colorScheme: ColorScheme.fromSeed(seedColor: Colors.deepPurple),
        useMaterial3: true,
      ),
      home: UserListPage(),
      // FutureBuilder(
      //   future: db.getDatabaseReference(),
      //   builder: (context, snapshot) {
      //     if (snapshot.hasData && snapshot.data != null) {
      //       return FutureBuilder<SharedPreferences>(
      //         future: SharedPreferences.getInstance(),
      //         builder: (context, snapshot1) {
      //           if (snapshot1.hasData && snapshot1.data != null) {
      //             if (snapshot1.data!.getBool('IsLogin') ?? false) {
      //               return StudentListPage();
      //             } else {
      //               return LoginPage();
      //             }
      //           } else {
      //             return const CircularProgressIndicator();
      //           }
      //         },
      //       );
      //     } else {
      //       return CircularProgressIndicator();
      //     }
      //   },
      // )
    );
  }
}