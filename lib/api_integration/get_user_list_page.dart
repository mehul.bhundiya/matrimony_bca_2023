import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:intl/intl.dart';
import 'package:matrimony/view/add_student_page.dart';

class UserListPage extends StatefulWidget {
  @override
  State<UserListPage> createState() => _UserListPageState();
}

class _UserListPageState extends State<UserListPage> {
  List<dynamic> userList = [];

  List<dynamic> filterUserList = [];

  bool isGetUserList = true;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          'User List',
          style: TextStyle(
            color: Colors.white,
          ),
        ),
        actions: [
          InkWell(
            child: Icon(
              Icons.add,
              color: Colors.white,
            ),
            onTap: () {
              Navigator.of(context).push(MaterialPageRoute(
                builder: (context) {
                  return AddStudentPage();
                },
              ));
            },
          ),
        ],
        backgroundColor: Colors.red,
      ),
      body: FutureBuilder<http.Response>(
        future: isGetUserList ? getUserList() : null,
        builder: (context, snapshot) {
          if (snapshot.hasData && snapshot.data != null) {
            if (isGetUserList) {
              userList.addAll(jsonDecode(snapshot.data!.body));
              filterUserList.addAll(userList);
            }
            isGetUserList = false;

            return Column(
              children: [
                TextFormField(
                  onChanged: (value) {
                    setState(() {
                      filterUserList.clear();
                      for (int i = 0; i < userList.length; i++) {
                        if (userList[i]['name']
                                .toString()
                                .toLowerCase()
                                .contains(value.toString().toLowerCase()) ||
                            userList[i]['city']
                                .toString()
                                .toLowerCase()
                                .contains(value.toString().toLowerCase())) {
                          filterUserList.add(userList[i]);
                        }
                      }
                    });
                  },
                ),
                Expanded(
                  child: ListView.builder(
                    itemBuilder: (context, index) {
                      return Card(
                        child: Padding(
                          padding: const EdgeInsets.only(
                              left: 12, right: 8, top: 8, bottom: 8),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text(
                                filterUserList[index]['name'].toString(),
                                style: TextStyle(
                                  color: Colors.black,
                                  fontSize: 20,
                                  fontWeight: FontWeight.bold,
                                ),
                              ),
                              Text(
                                getFormattedDate(
                                  filterUserList[index]['Dob'].toString(),
                                ),
                                style: TextStyle(
                                  color: Colors.grey,
                                  fontSize: 16,
                                ),
                              ),
                            ],
                          ),
                        ),
                      );
                    },
                    itemCount: filterUserList.length,
                  ),
                ),
              ],
            );
          } else {
            return Center(child: CircularProgressIndicator());
          }
        },
      ),
    );
  }

  String getFormattedDate(String date) {
    String formattedDate;
    try {
      DateTime dateTime = DateFormat("yyyy-MM-ddTHH:mm:ssZ").parse(date);
      formattedDate = DateFormat('dd-MMM-yyyy').format(dateTime);
    } catch (e) {
      DateTime dateTime = DateFormat("dd/MM/yyyy").parse(date);
      formattedDate = DateFormat('dd-MMM-yyyy').format(dateTime);
    }
    return formattedDate;
  }

  Future<http.Response> getUserList() async {
    http.Response res = await http
        .get(Uri.parse('https://638029948efcfcedacfe0228.mockapi.io/api/user'));
    return res;
  }
}
