import 'dart:io';

import 'package:matrimony/model/user_model.dart';
import 'package:path_provider/path_provider.dart';
import 'package:sqflite/sqflite.dart';
import 'package:path/path.dart';

class DemoDatabase {
  static String TBL_USER = 'Tbl_User';
  static String USER_ID = 'UserID';
  static String NAME = 'Name';
  static String CITY = 'City';

  Future<Database> getDatabaseReference() async {
    Directory dir = await getApplicationDocumentsDirectory();
    return await openDatabase(
      join(dir.path, 'Demo.db'),
      version: 1,
      onCreate: (db, version) {
        db.execute(
            'CREATE TABLE Tbl_User(UserID INTEGER PRIMARY KEY AUTOINCREMENT, Name TEXT, City TEXT)');
      },
    );
  }

  Future<int> insertUserInTblUser(
      {required String name, required String city}) async {
    Map<String, dynamic> map = {};
    map['Name'] = name;
    map['City'] = city;
    Database db = await getDatabaseReference();
    return await db.insert('Tbl_User', map);
  }

  Future<int> updateUserInTblUser(
      {required String name, required String city, required int userId}) async {
    Map<String, dynamic> map = {};
    map['Name'] = name;
    map['City'] = city;
    Database db = await getDatabaseReference();
    return await db
        .update('Tbl_User', map, where: 'UserID = ?', whereArgs: [userId]);
  }

  Future<int> deleteUserFromTbleUser({required int userId}) async {
    Database db =await getDatabaseReference();
    return await db.delete('Tbl_User', where: 'UserID = ?', whereArgs: [userId]);
  }

  Future<List<UserModel>> getDataFromTblUser() async {
    Database db = await getDatabaseReference();
    List<Map<String, Object?>> userList =
        await db.rawQuery('SELECT * FROM $TBL_USER');
    return List.generate(
      userList.length,
      (index) => UserModel.name(
        userList[index][USER_ID] as int,
        userList[index][NAME].toString(),
        userList[index][CITY].toString(),
      ),
    );
  }
}
