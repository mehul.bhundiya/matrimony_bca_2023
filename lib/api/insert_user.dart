import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;

class InsertUser extends StatelessWidget {
  TextEditingController nameController = TextEditingController();
  TextEditingController dobController = TextEditingController();
  TextEditingController cityController = TextEditingController();

  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text(
          'Insert User',
        ),
      ),
      body: Form(
        key: _formKey,
        child: Column(
          children: [
            TextFormField(
              controller: nameController,
              validator: (value) {
                if (value!.isEmpty) {
                  return 'Enter Name';
                }
                return null;
              },
            ),
            TextFormField(
              controller: dobController,
              validator: (value) {
                if (value!.isEmpty) {
                  return 'Enter DOB';
                }
                return null;
              },
            ),
            TextFormField(
              controller: cityController,
              validator: (value) {
                if (value!.isEmpty) {
                  return 'Enter City';
                }
                return null;
              },
            ),
            TextButton(
              onPressed: () {
                if (_formKey.currentState!.validate()) {}
              },
              child: const Text('Save'),
            ),
          ],
        ),
      ),
    );
  }

  Future<http.Response> insertUser() async {
    Map<String, dynamic> map = {};
    map['name'] = nameController.value.text;
    map['Dob'] = dobController.value.text;
    map['City'] = cityController.value.text;
    http.Response res = await http.post(
      Uri.parse(
        'https://638029948efcfcedacfe0228.mockapi.io/api/user',
      ),
      body: map,
    );
    return res;
  }
}
