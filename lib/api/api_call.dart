import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:http/http.dart' as http;
import 'package:intl/intl.dart';
import 'package:matrimony/api/insert_user.dart';
import 'package:matrimony/view/add_user.dart';
import 'package:matrimony/view/login_page.dart';
import 'package:matrimony/view/user_list.dart';

class ApiCall extends StatefulWidget {
  const ApiCall({super.key});

  @override
  State<ApiCall> createState() => _ApiCallState();
}

class _ApiCallState extends State<ApiCall> {
  List<dynamic> userList = [];

  int index = 0;
  bool isListSelected = true;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.pink,
        title: const Text(
          'Api Call',
          style: TextStyle(
            color: Colors.white,
            fontSize: 18,
            fontWeight: FontWeight.bold,
          ),
        ),
        actions: [
          InkWell(
            onTap: () {
              Navigator.of(context).push(
                MaterialPageRoute(
                  builder: (context) {
                    return InsertUser();
                  },
                ),
              );
            },
            child: Icon(
              Icons.add,
              color: Colors.white,
            ),
          ),
        ],
      ),
      drawer: NavigationDrawer(
        children: [
          DrawerHeader(
            decoration: BoxDecoration(
              color: Colors.red,
            ),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Expanded(
                  child: CircleAvatar(
                    radius: 50,
                    foregroundImage: AssetImage(
                      'assets/images/bg_matrimony_prelogin.jpg',
                    ),
                  ),
                ),
                Text(
                  'Prof. Mehul Bhundiya',
                  style: TextStyle(
                    color: Colors.white,
                    fontWeight: FontWeight.bold,
                  ),
                ),
                Text(
                  'mehul.bhundiya@darshan.ac.in',
                  style: TextStyle(
                    fontSize: 12,
                    color: Colors.white60,
                  ),
                ),
              ],
            ),
          ),
          getListTile(selectedIndex: 0, title: 'Home'),
          Divider(),
          getListTile(selectedIndex: 1, title: 'User List'),
          Divider(),
          getListTile(selectedIndex: 2, title: 'Add User'),
          Divider(),
          getListTile(selectedIndex: 3, title: 'Login'),
        ],
      ),
      body: getSelectedWidgetPage(),
      bottomNavigationBar: BottomNavigationBar(
        selectedItemColor: Colors.yellow,
        selectedFontSize: 20,
        unselectedFontSize: 12,
        unselectedItemColor: Colors.white,
        items: [
          BottomNavigationBarItem(
            icon: Icon(Icons.home),
            label: 'Home',
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.list),
            label: 'User List',
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.add),
            label: 'Add User',
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.login),
            label: 'Login',
          ),
        ],
        currentIndex: index,
        onTap: (value) {
          setState(() {
            index = value;
          });
        },
        backgroundColor: Colors.red,
        type: BottomNavigationBarType.fixed,
      ),
    );
  }

  Widget getListTile({required int selectedIndex, required String title}) {
    return ListTile(
      onTap: () {
        Navigator.pop(context);
        setState(() {
          index = selectedIndex;
        });
      },
      title: Text(
        title,
      ),
    );
  }

  Widget getSelectedWidgetPage() {
    if (index == 0) {
      return Column(
        children: [
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.end,
              children: [
                InkWell(
                  child: Icon(
                    Icons.list,
                    color: isListSelected ? Colors.blue : Colors.black,
                  ),
                  onTap: () {
                    setState(() {
                      isListSelected = !isListSelected;
                    });
                  },
                ),
                Text(
                  '  /  ',
                  style: TextStyle(fontSize: 25),
                ),
                InkWell(
                  child: Icon(
                    Icons.grid_on,
                    color: isListSelected ? Colors.black : Colors.blue,
                  ),
                  onTap: () {
                    setState(() {
                      isListSelected = !isListSelected;
                    });
                  },
                ),
              ],
            ),
          ),
          Expanded(
            child: FutureBuilder(
              future: getUsersListFromApi(),
              builder: (context, snapshot) {
                if (snapshot.hasData && snapshot.data != null) {
                  userList = jsonDecode(snapshot.data.toString());
                  return isListSelected
                      ? ListView.builder(
                          itemBuilder: (context, index) {
                            return getListGridItem(index);
                          },
                          itemCount: userList.length,
                        )
                      : GridView.builder(
                          gridDelegate:
                              const SliverGridDelegateWithFixedCrossAxisCount(
                                  crossAxisCount: 5),
                          itemCount: userList.length,
                          itemBuilder: (context, index) {
                            return getListGridItem(index);
                          },
                        );
                } else {
                  return const Center(child: CircularProgressIndicator());
                }
              },
            ),
          ),
        ],
      );
    } else if (index == 1) {
      return UserListPage();
    } else if (index == 2) {
      return AddUser();
    } else {
      return LoginPage();
    }
  }

  Widget getListGridItem(int dataIndex) {
    return Card(
      elevation: 10,
      margin: EdgeInsets.all(10),
      child: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              userList[dataIndex]['name'].toString(),
              style: TextStyle(fontSize: 25),
            ),
            Text(
              getFormattedDate(userList[dataIndex]['Dob'].toString()),
              style: TextStyle(
                fontSize: 20,
                color: Colors.black,
              ),
            ),
            Text(
              userList[dataIndex]['City'].toString(),
              style: TextStyle(
                fontSize: 15,
                color: Colors.grey,
              ),
            ),
          ],
        ),
      ),
    );
  }

  String getFormattedDate(String date) {
    DateTime dateTime = DateFormat("yyyy-MM-ddTHH:mm:ssZ").parse(date);
    String formattedDate = DateFormat('dd-MMM-yyyy').format(dateTime);
    return formattedDate;
  }

  Future<String> getUsersListFromApi() async {
    http.Response res = await http.get(
      Uri.parse('https://638029948efcfcedacfe0228.mockapi.io/api/user'),
    );
    return res.body.toString();
  }
}