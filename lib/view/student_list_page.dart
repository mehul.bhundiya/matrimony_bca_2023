import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:matrimony/model/student_model.dart';
import 'package:matrimony/view/add_student_page.dart';
import 'package:matrimony/view/my_database.dart';

class StudentListPage extends StatefulWidget {
  @override
  State<StudentListPage> createState() => _StudentListPageState();
}

class _StudentListPageState extends State<StudentListPage> {
  MyDatabase db = MyDatabase();

  List<StudentModel> studentList = [];
  List<StudentModel> filteredList = [];
  bool isGetStudentList = true;
  TextEditingController filterController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      drawer: Container(color: Colors.white),
      endDrawer: Container(),
      appBar: AppBar(
        backgroundColor: Colors.pink,
        title: const Text(
          'User List',
          style: TextStyle(
            color: Colors.white,
            fontSize: 16,
          ),
        ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          Navigator.of(context).push(
            MaterialPageRoute(
              builder: (context) {
                return AddStudentPage();
              },
            ),
          ).then(
            (value) {
              if (value) {
                setState(() {});
              }
            },
          );
        },
        backgroundColor: Colors.pink,
        child: const Icon(
          Icons.add,
          color: Colors.white,
        ),
      ),
      body: FutureBuilder(
        future: isGetStudentList ? db.getStudentList() : null,
        builder: (context, snapshot) {
          if (snapshot.hasData && snapshot.data != null) {
            if (snapshot.data!.isNotEmpty) {
              if (isGetStudentList) {
                studentList.clear();
                filteredList.clear();
                studentList.addAll(snapshot.data!);
                filteredList.addAll(studentList);
              }
              isGetStudentList = false;
              return Padding(
                padding: const EdgeInsets.all(8.0),
                child: Column(
                  children: [
                    TextFormField(
                      controller: filterController,
                      decoration: InputDecoration(
                        suffixIcon: InkWell(
                          onTap: () {
                            setState(() {
                              filterController.clear();
                              isGetStudentList = true;
                            });
                          },
                          child: Icon(
                            Icons.clear,
                          ),
                        ),
                        hintText: 'Type Name|Enroll. to Search.',
                        border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(
                            50,
                          ),
                          borderSide: BorderSide(color: Colors.pink),
                        ),
                        focusedBorder: OutlineInputBorder(
                            borderSide: BorderSide(color: Colors.red),
                            borderRadius: BorderRadius.circular(100)),
                        prefixIcon: Icon(
                          Icons.search,
                        ),
                      ),
                      onChanged: (value) {
                        filteredList.clear();
                        for (int i = 0; i < studentList.length; i++) {
                          if (studentList[i]
                                  .Name
                                  .toString()
                                  .toLowerCase()
                                  .contains(value.toLowerCase()) ||
                              studentList[i]
                                  .EnrollmentNo
                                  .toString()
                                  .toLowerCase()
                                  .contains(value.toLowerCase())) {
                            filteredList.add(studentList[i]);
                          }
                          setState(() {});
                        }
                      },
                    ),
                    Expanded(
                      child: ListView.builder(
                        itemCount: filteredList.length,
                        itemBuilder: (context, index) {
                          return InkWell(
                            onTap: () {
                              Navigator.of(context).push(
                                MaterialPageRoute(
                                  builder: (context) {
                                    return AddStudentPage(
                                      name: filteredList[index].Name.toString(),
                                      enrollmentNo: filteredList[index]
                                          .EnrollmentNo
                                          .toString(),
                                      studentId: filteredList[index].StudentId,
                                    );
                                  },
                                ),
                              ).then(
                                (value) {
                                  if (value) {
                                    setState(() {});
                                  }
                                },
                              );
                            },
                            child: Card(
                              color: Colors.white,
                              child: Padding(
                                padding: const EdgeInsets.all(8.0),
                                child: Row(
                                  children: [
                                    Container(
                                      width: 3,
                                      color: filteredList[index]
                                                  .Name
                                                  .toString()
                                                  .characters
                                                  .first
                                                  .toLowerCase() ==
                                              'r'
                                          ? Colors.green
                                          : Colors.red,
                                      height: 25,
                                    ),
                                    Expanded(
                                      child: Padding(
                                        padding: const EdgeInsets.only(left: 8),
                                        child: Column(
                                          crossAxisAlignment:
                                              CrossAxisAlignment.start,
                                          children: [
                                            Text(
                                              filteredList[index]
                                                  .Name
                                                  .toString(),
                                              style: const TextStyle(
                                                color: Colors.blue,
                                                fontWeight: FontWeight.bold,
                                              ),
                                            ),
                                            Text(
                                              filteredList[index]
                                                  .EnrollmentNo
                                                  .toString(),
                                              style: const TextStyle(
                                                color: Colors.grey,
                                                fontSize: 12,
                                              ),
                                            ),
                                          ],
                                        ),
                                      ),
                                    ),
                                    InkWell(
                                      onTap: () {
                                        showCupertinoDialog(
                                          context: context,
                                          builder: (context) {
                                            return CupertinoAlertDialog(
                                              title: Text(
                                                'Are you sure want to delete?',
                                              ),
                                              actions: [
                                                TextButton(
                                                  onPressed: () async {
                                                    int deletedID = await db
                                                        .deleteStudentFromTblStudent(
                                                      studentID: snapshot
                                                          .data![index]
                                                          .StudentId!,
                                                    );
                                                    print('ID :: $deletedID');
                                                    print(
                                                        'ID2 :: ${filteredList[index].StudentId!}');
                                                    Navigator.of(context).pop();
                                                    if (deletedID == 1) {
                                                      setState(() {});
                                                    }
                                                  },
                                                  child: Text('Yes'),
                                                ),
                                                TextButton(
                                                  onPressed: () {
                                                    Navigator.of(context).pop();
                                                  },
                                                  child: Text('No'),
                                                ),
                                              ],
                                            );
                                          },
                                        );
                                      },
                                      child: Icon(
                                        Icons.delete,
                                        color: Colors.red,
                                      ),
                                    ),
                                    Icon(
                                      Icons.arrow_forward_ios_rounded,
                                      size: 13,
                                      color: Colors.grey,
                                    ),
                                  ],
                                ),
                              ),
                            ),
                          );
                        },
                      ),
                    ),
                  ],
                ),
              );
            } else {
              return const Center(
                child: Text(
                  'No Student Found',
                  style: TextStyle(
                    color: Colors.grey,
                  ),
                ),
              );
            }
          } else {
            return const CircularProgressIndicator();
          }
        },
      ),
    );
  }
}
