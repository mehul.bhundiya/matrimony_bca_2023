import 'package:flutter/material.dart';

class AddGroceryPage extends StatefulWidget {
  String? name;
  String? message;
  String? imageUrl;
  int? counter;

  AddGroceryPage({
    this.name,
    this.message,
    this.imageUrl,
    this.counter,
  });

  @override
  State<AddGroceryPage> createState() => _AddGroceryPageState();
}

class _AddGroceryPageState extends State<AddGroceryPage> {
  TextEditingController nameTextController = TextEditingController();
  TextEditingController messageTextController = TextEditingController();
  TextEditingController imageTextController = TextEditingController();
  TextEditingController counterTextController = TextEditingController();

  @override
  void initState() {
    super.initState();
    nameTextController.text = widget.name ?? '';
    messageTextController.text = widget.message ?? '';
    imageTextController.text = widget.imageUrl ?? '';
    counterTextController.text = (widget.counter ?? 0).toString();
  }

  GlobalKey<FormState> _formKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(backgroundColor: Colors.green),
      body: Form(
        key: _formKey,
        child: Column(
          children: [
            TextFormField(
              controller: nameTextController,
              validator: (value) {
                if (value == null || value.isEmpty) {
                  return 'Enter Name';
                }
              },
            ),
            TextFormField(
              controller: messageTextController,
              validator: (value) {
                if (value == null || value.isEmpty) {
                  return 'Enter Message';
                }
              },
            ),
            TextFormField(
              controller: imageTextController,
              validator: (value) {
                if (value == null || value.isEmpty) {
                  return 'Enter Image URL';
                }
              },
            ),
            TextFormField(
              controller: counterTextController,
            ),
            TextButton(
              onPressed: () {
                if (_formKey.currentState!.validate()) {
                  Map<String, dynamic> whatsappItem = {};
                  whatsappItem['ImageUrl'] =
                      imageTextController.text.toString();
                  whatsappItem['UserName'] = nameTextController.text.toString();
                  whatsappItem['Message'] =
                      messageTextController.text.toString();
                  whatsappItem['Counter'] =
                      int.parse(counterTextController.text.toString());
                  Navigator.of(context).pop(whatsappItem);
                }
              },
              child: Text('Submit'),
            ),
          ],
        ),
      ),
    );
  }
}
