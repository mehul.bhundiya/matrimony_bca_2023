import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:matrimony/view/my_database.dart';

class AddStudentPage extends StatefulWidget {
  String? name, enrollmentNo;
  int? studentId;

  AddStudentPage({super.key, this.name, this.enrollmentNo, this.studentId});

  @override
  State<AddStudentPage> createState() => _AddStudentPageState();
}

class _AddStudentPageState extends State<AddStudentPage> {
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();

  TextEditingController nameController = TextEditingController();

  TextEditingController enrollmentController = TextEditingController();

  MyDatabase db = MyDatabase();

  @override
  void initState() {
    super.initState();
    nameController.text = widget.name ?? '';
    enrollmentController.text = widget.enrollmentNo ?? '';
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.pink,
        title: const Text(
          'Add Student',
          style: TextStyle(
            color: Colors.white,
            fontSize: 16,
          ),
        ),
      ),
      body: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Form(
          key: _formKey,
          child: Column(children: [
            TextFormField(
              controller: nameController,
              decoration: InputDecoration(
                hintText: 'Enter Name',
                border: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(
                    15,
                  ),
                ),
              ),
              validator: (value) {
                if (value!.isEmpty) {
                  return 'Enter Valid Name';
                }
                return null;
              },
            ),
            const SizedBox(
              height: 10,
            ),
            InkWell(
              onTap: () {
                showDatePickerDialogCustom(context);
              },
              child: TextFormField(
                controller: enrollmentController,
                enabled: false,
                validator: (value) {
                  if (value!.isEmpty) {
                    return 'Enter Enrollment No.';
                  }
                  if (value.isNotEmpty && value.length != 10) {
                    return 'Enter Valid Enrollment No.';
                  }
                  return null;
                },
                decoration: InputDecoration(
                  hintText: 'Enter Enrollment No.',
                  border: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(
                      15,
                    ),
                  ),
                ),
              ),
            ),
            TextButton(
              onPressed: () async {
                if (_formKey.currentState!.validate()) {}
              },
              style: const ButtonStyle(
                backgroundColor: MaterialStatePropertyAll(
                  Colors.pink,
                ),
              ),
              child: const Text(
                'Save',
                style: TextStyle(color: Colors.white),
              ),
            )
          ]),
        ),
      ),
    );
  }

  void showDatePickerDialogCustom(context) {
    showDialog(
      context: context,
      builder: (context) {
        return DatePickerDialog(
          initialDate: DateTime.now(),
          firstDate: DateTime.now().add(const Duration(days: -365)),
          lastDate: DateTime.now().add(const Duration(days: 365)),
        );
      },
    );
  }
}
